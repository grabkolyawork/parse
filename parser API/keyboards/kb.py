from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

b1 = KeyboardButton('/start')
b2 = KeyboardButton('/ports')
b3 = KeyboardButton('/append')
b4 = KeyboardButton('/parse')

kbuttons = ReplyKeyboardMarkup(resize_keyboard=True)

kbuttons.row(b1,b2,b3).add(b4)