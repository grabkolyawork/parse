from aiogram.utils import executor
from bot_create import dp

from handlers import bot_info, server_info, server_parse

'''=Информирование что бот запустился='''
async def on_startup(_):
	print('Bot started')

bot_info.register_handlers_botinfo(dp)
server_info.register_handlers_serverinfo(dp)
server_parse.register_handlers_serverparse(dp)

executor.start_polling(dp, skip_updates=True, on_startup=on_startup)