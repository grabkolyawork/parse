from aiogram import types, Dispatcher
from bot_create import dp, bot
from keyboards import kbuttons
'''=============Список информации=============='''
info = ['/start : начало работы\n','/ports : актуальный список серверов\n','/append : добавление нового сервера\n','/parse : парсинг информации с выбранного сервера']
'''===============================FAQ====================================='''
async def commands_start(message : types.Message):
	try:
		await bot.send_message(message.from_user.id, 'Список команд: '+ str(info), reply_markup=kbuttons)
		await message.delete()
	except:
		await message.reply('Напишите в ЛС, если бот не сработал, вот он @F_Words_Count_bot:')

def register_handlers_botinfo(dp : Dispatcher):
	dp.register_message_handler(commands_start, commands=['start'])