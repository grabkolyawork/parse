from aiogram import types, Dispatcher
from bot_create import dp, bot
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

'''=============================Получение списка серверов================================'''
async def Create_Server_List(message: types.Message):
	Server_List = []
	with open('servers.ini') as f:
		size=len([0 for _ in f])
	with open('servers.ini') as f:
		text = f.readlines()
		for i in range(size):
			Server_List.append((text[i].split(':'))[0])
	await bot.send_message(message.from_user.id, 'Доступные порты серверов:' + str(Server_List))
'''===============================Добавление сервера========================================='''
'''=========Стейк-машина для запоминания нового сервера============='''
class FSMServer(StatesGroup):
	code_server = State()
	url_server = State()

async def Start_Server_Info(message : types.Message):
	await FSMServer.code_server.set()
	await message.reply('Введите код сервера:')

async def CServer(message : types.Message, state=FSMContext):
	async with state.proxy() as data:
		data['code_server'] = message.text
	await FSMServer.next()
	await message.reply('Введите url сервера:')

async def UServer(message : types.Message, state=FSMContext):
	async with state.proxy() as data:
		data['url_server'] = message.text
	with open('servers.ini', 'a') as f:
			f.write('\n' + data['code_server'] + ':' + data['url_server'])
	await message.reply('Данные сервера успешно добавленны!:')
	await state.finish()

def register_handlers_serverinfo(dp : Dispatcher):
	dp.register_message_handler(Create_Server_List, commands=['ports'])
	dp.register_message_handler(Start_Server_Info, commands=['append'])
	dp.register_message_handler(CServer, state=FSMServer.code_server)
	dp.register_message_handler(UServer, state=FSMServer.url_server)