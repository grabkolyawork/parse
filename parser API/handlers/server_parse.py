from aiogram import types, Dispatcher
from bot_create import dp
from bs4 import BeautifulSoup
import urllib.request
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
import time

'''================================Парсинг на новые задачи==================================='''
async def parser(message : types.Message, url):
	html = urllib.request.urlopen(url).read()
	soup = BeautifulSoup(html, 'lxml')
	server = soup.find_all('p')
	server_list = []

	for i in server:
		server_list.append(str(i))
		buf = server_list[0].split('{')

	server_list = []
	for i in range(2,len(buf)):
		server_list.append(buf[i])

	list_data_servers = []
	for i in range(len(server_list)):
		one_server_data = []
		#Сервер
		one_server_data.append(server_list[i].split(':')[4].split(',')[0].split('"')[1])
		#№ модема
		one_server_data.append(server_list[i].split(':')[5].split(',')[0].split('.')[2])
		#Город
		one_server_data.append(server_list[i].split(':')[6].split(',')[0].split('"')[1])
		#Оператор
		one_server_data.append(server_list[i].split(':')[7].split(',')[0].split('}',-1)[0].split('"')[1])
		#Время
		one_server_data.append(server_list[i].split(':')[1].split(',')[0] + ' ' + server_list[i].split(':')[1].split(',')[1]
												+ ':' + server_list[i].split(':')[2].split(',')[0]
												+ ':' + server_list[i].split(':')[3].split(',')[0])

		dict_data_servers = {'Server' : one_server_data[0],
									'Modem' : one_server_data[1],
									'City' : one_server_data[2],
									'Operator' : one_server_data[3],
									'DataTime' : one_server_data[4]
									}
		list_data_servers.append(dict_data_servers)
		#создаем файл с данными сервера если его нет
		#если файл есть проверяем если запись в нем и записываем если нет
		with open(str(dict_data_servers['Server'])+'.ini', 'a+', encoding='utf-8') as f:
			f.seek(0)
			if str(dict_data_servers['DataTime']) not in f.read():
				f.write(str(dict_data_servers)+'\n')
				await message.reply('Сервер: ' + list_data_servers[i]['Server'] + '\n' + 'Модем: ' + list_data_servers[i]['Modem'] + '\n' + 'Город: ' + list_data_servers[i]['City'] + '\n' + 'Оператор: ' + list_data_servers[i]['Operator'] + '\n' + 'Дата: ' + list_data_servers[i]['DataTime'] + '\n\n')
'''=========Спрашиваем сервер и запоминаем его======================='''
class FSMParser(StatesGroup):
	url_for_parse = State()

async def StartParsing(message : types.Message):
	await FSMParser.url_for_parse.set()
	await message.answer('Введите код сервера:')

async def Save_Url(message : types.Message, state=FSMContext):
	async with state.proxy() as data:
		data['url_for_parse'] = message.text
		with open('servers.ini') as f:
			size=len([0 for _ in f])
		with open('servers.ini') as f:
			text = f.readlines()
			for i in range(size):
				if (text[i].split(':'))[0] == data['url_for_parse']:
					url = (text[i].split(':'))[1] + ':' + (text[i].split(':'))[2]
	while True:
		await parser(message, url)
		time.sleep(1)
		await StartParsing(message)
	await state.finish()


def register_handlers_serverparse(dp : Dispatcher):
	dp.register_message_handler(StartParsing, commands=['parse'])
	dp.register_message_handler(Save_Url, state=FSMParser.url_for_parse)
	dp.register_message_handler(parser)